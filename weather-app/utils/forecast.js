const request = require('postman-request')

const forecast = (latitude, longitude, callback) => {
    const url = `http://api.weatherstack.com/current?access_key=11f335b12819320b2f3e710ee3f89a6b&query=${latitude},${longitude}`

    request({ url, json: true}, (error, { body } = {}) => {
        if (error) {
            callback('Unable to connect with weather service!', undefined)
        }
        else if (body.error) {
            callback('Unable to find location.', undefined)
        } else {

            const {temperature, precip} = body.current

            callback(undefined, `It is currently ${temperature} degrees out. There is a ${precip}% chance of rain.`
            )
        }
    })
}

module.exports = forecast
